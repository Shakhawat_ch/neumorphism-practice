//
//  SimpleButtonStyle.swift
//  Neumorphism-practice
//
//  Created by Shakhawat Hossain Shahin on 9/10/21.
//

import Foundation
import SwiftUI

struct SimpleButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
    }
}
