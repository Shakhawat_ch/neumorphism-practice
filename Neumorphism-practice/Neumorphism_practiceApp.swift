//
//  Neumorphism_practiceApp.swift
//  Neumorphism-practice
//
//  Created by Shakhawat Hossain Shahin on 9/10/21.
//

import SwiftUI

@main
struct Neumorphism_practiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
