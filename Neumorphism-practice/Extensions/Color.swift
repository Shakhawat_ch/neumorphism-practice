//
//  Color.swift
//  Neumorphism-practice
//
//  Created by Shakhawat Hossain Shahin on 9/10/21.
//

import Foundation
import SwiftUI

extension Color {
    static let offWhite = Color(red: 225 / 255, green: 225 / 255, blue: 235 / 255)
}
